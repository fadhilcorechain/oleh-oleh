package main

import (
	"github.com/go-redis/redis"
	"fmt"
	"net/http"
	"io/ioutil"
	"github.com/buger/jsonparser"
	"encoding/base64"
	"os"
	"io"
	"github.com/minio/minio-go"
	"log"
	"time"
	"net/url"
	"database/sql"
	_"github.com/lib/pq"
	"encoding/json"
	"sort"
	"bytes"
)

const PORT_NUMBER = 6023
const BUCKET_NAME = "oleh-oleh"

const DB_URL  = "178.128.22.97:26257"
const EXP_TIME = time.Second*168*60*60

type Gifts struct{
	IdLocation string `json:"idLocation"`
	IdImage []string `json:"idImage"`
}

type Datas struct {
	IdLocation string `json:"idLocation"`
	Id string `json:"id"`
	Namelocation string `json:"nameLocation"`
	Image []DataImage
}

type DataImage struct {
	IdGifts int `json:"idGifts"`
	IdImage int `json:"idImage"`
	Nameimage string `json:"name"`
	Url string `json:"url"`
	Filename string `json:"filename"`
	Description string `json:"description"`
}

type UserGifts struct {
	Id int `json:"id"`
	PublicKey string `json:"publicKey"`
	IdGifts string `json:"idGifts"`
	Total string `json:"total"`
	Timestamp int64 `json:"timestamp"`
	IdImage int `json:"idImage"`
	Filename string `json:"filename"`
	Name string `json:"name"`
	Url string `json:"url"`
}

func initDb()(db *sql.DB){
	db, err := sql.Open("postgres", "postgresql://root@"+DB_URL+"/gifts?sslmode=disable")
	if err != nil {
		log.Fatal("error connecting : ", err)
	}
	return db
}

func conn() *redis.Client {
	client := redis.NewClient(&redis.Options{
		//Addr: "localhost:6379",
		//Addr:     "35.197.155.228:9967",
		Addr:     "35.194.1.0:6379",
		Password: "t3guhCakep", // no password set
		DB:       7,  // use default DB
	})
	return client
}

func initMinio() *minio.Client{
	endpoint:="103.43.44.157:9000"
		accessKeyID := "VT2JFACT2ORPFN2CLBB8"
	secretAccessKey := "drBAZkNb4GvnTbO54hAxiOG9DdQmBj5LfERyUR/T"
	useSSL := false
	minioClient, err := minio.NewV2(endpoint, accessKeyID, secretAccessKey, useSSL)
	if err != nil {
		log.Fatalln(err)
		return nil
	}else{
		return minioClient
	}
}


func main() {
	fmt.Println("==== OTP services ====")
	fmt.Println(fmt.Sprintf(" Run on Port :%d", PORT_NUMBER))
	fmt.Println("======================")
	rdb := conn()
	defer rdb.Close()
	db := initDb()
	defer db.Close()
	minio := initMinio()
	mux := http.NewServeMux()
	mux.HandleFunc("/health", health)
	mux.HandleFunc("/apiv1/gift/list/all", findAllGift(db,minio))
	mux.HandleFunc("/apiv1/gift/list/province", findByProvince(db,minio))
	mux.HandleFunc("/apiv1/gift/list/user", findByUser(db,minio))
	mux.HandleFunc("/apiv1/gift/checkLastGift", findLastGiftByUser(db))
	mux.HandleFunc("/apiv1/gift/add", addImage(db,minio))
	mux.HandleFunc("/apiv1/gift/update", updateImage(db,minio))
	mux.HandleFunc("/apiv1/gift/remove", removeImage(db,minio))
	mux.HandleFunc("/apiv1/gift/setLocation", setGiftsLocation(db,minio))
	mux.HandleFunc("/apiv1/gift/setGifts", setGiftUsers(db,minio))
	mux.HandleFunc("/apiv1/gift/upload", imageUploader())
	mux.HandleFunc("/apiv1/gift/uploadBinary", imageUploaderBinaryFile())
	http.ListenAndServe(fmt.Sprintf(":%d", PORT_NUMBER), mux)
}

func health(w http.ResponseWriter, r *http.Request){
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{"message":"HEALTH"}`))
}

func findAllGift(db *sql.DB,minioClient *minio.Client) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request) {
		code := http.StatusInternalServerError
		message := `{"message":"error get data"}`

		querySelect := `SELECT b.id as id_gifts ,a.id_location,a.id,a.nameLocation,c.id as idImage,c.name as nameImage,c.filename FROM (
    SELECT id_location,id_province as id,province_name as nameLocation FROM tb_province
    UNION
    SELECT id_location,id_city,concat(city_type,' ',city_name) FROM tb_city
    UNION
    SELECT id_location,id_subdistrict,subdistrict_name FROM tb_subdistrict
    UNION
    SELECT id_location,id_village,village_name FROM tb_village
) a INNER JOIN tb_gifts b ON a.id_location = b.id_location LEFT JOIN tb_image c ON b.id_image = c.id`

	rows,err := db.Query(querySelect)
	if err != nil {
		message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
		w.WriteHeader(code)
		w.Write([]byte(message))
		return
	}
	defer rows.Close()

	objMaps := map[string]Datas{}

	for rows.Next(){
		var obj Datas
		var objImage DataImage

		err := rows.Scan(&objImage.IdGifts,&obj.IdLocation,&obj.Id,&obj.Namelocation,&objImage.IdImage,&objImage.Nameimage,&objImage.Filename)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}

		url, err := minioClient.PresignedGetObject(BUCKET_NAME, objImage.Filename, EXP_TIME, nil)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}

		objImage.Url = url.String()

		if len(objMaps[obj.IdLocation].Id) <= 0{
			obj.Image = append(obj.Image,objImage)
			objMaps[obj.IdLocation] = obj
		}else{
			existObj := objMaps[obj.IdLocation].Image
			existObj = append(existObj,objImage)
			obj.Image = existObj
			objMaps[obj.IdLocation] = obj
		}
	}

		// Sorting idProvince if needed
		var keys []string
		for key := range objMaps{
			keys = append(keys,key)
		}
		sort.Strings(keys)

		var objs []Datas
		for _,key := range keys{
			var obj Datas
			var objImages []DataImage
			obj.IdLocation = key
			obj.Id= objMaps[key].Id
			obj.Namelocation = objMaps[key].Namelocation
			for _,val := range objMaps[key].Image{
				var objImage DataImage
				objImage.IdGifts = val.IdGifts
				objImage.IdImage = val.IdImage
				objImage.Nameimage = val.Nameimage
				objImage.Url = val.Url
				objImage.Filename = val.Filename
				objImage.Description = val.Description
				objImages = append(objImages,objImage)
			}
			obj.Image = objImages
			objs = append(objs,obj)
		}

		jsonResp,err := json.Marshal(objs)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`,err.Error())
			return
		}
		code = http.StatusOK
		message = string(jsonResp)
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func findByProvince(db *sql.DB,minioClient *minio.Client) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request) {
		code := http.StatusInternalServerError
		message := `{"message":"error get data"}`

		body,_ := ioutil.ReadAll(r.Body)
		idLocation,_ := jsonparser.GetString(body,"idLocation")

		queryCond := ``
		if(len(idLocation) > 0){
			queryCond = fmt.Sprintf(`and a.id_location = '%s'`,idLocation)
		}

		querySelect := fmt.Sprintf(`SELECT b.id as id_gifts ,a.id_location,a.id,a.nameLocation,c.id as idImage,c.name as nameImage,c.filename FROM (
    SELECT id_location,id_province as id,province_name as nameLocation FROM tb_province
    UNION
    SELECT id_location,id_city,concat(city_type,' ',city_name) FROM tb_city
    UNION
    SELECT id_location,id_subdistrict,subdistrict_name FROM tb_subdistrict
    UNION
    SELECT id_location,id_village,village_name FROM tb_village
) a INNER JOIN tb_gifts b ON a.id_location = b.id_location LEFT JOIN tb_image c ON b.id_image = c.id  WHERE 1 = 1 %s`,queryCond)

		rows,err := db.Query(querySelect)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}
		defer rows.Close()

		objMaps := map[string]Datas{}

		for rows.Next(){
			var obj Datas
			var objImage DataImage

			err := rows.Scan(&objImage.IdGifts,&obj.IdLocation,&obj.Id,&obj.Namelocation,&objImage.IdImage,&objImage.Nameimage,&objImage.Filename)
			if err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}

			url, err := minioClient.PresignedGetObject(BUCKET_NAME, objImage.Filename, EXP_TIME, nil)
			if err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}

			objImage.Url = url.String()

			if len(objMaps[obj.IdLocation].Id) <= 0{
				obj.Image = append(obj.Image,objImage)
				objMaps[obj.IdLocation] = obj
			}else{
				existObj := objMaps[obj.IdLocation].Image
				existObj = append(existObj,objImage)
				obj.Image = existObj
				objMaps[obj.IdLocation] = obj
			}
		}

		// Sorting idProvince if needed
		var keys []string
		for key := range objMaps{
			keys = append(keys,key)
		}
		sort.Strings(keys)

		var objs []Datas
		for _,key := range keys{
			var obj Datas
			var objImages []DataImage
			obj.IdLocation = key
			obj.Id= objMaps[key].Id
			obj.Namelocation = objMaps[key].Namelocation
			for _,val := range objMaps[key].Image{
				var objImage DataImage
				objImage.IdGifts = val.IdGifts
				objImage.IdImage = val.IdImage
				objImage.Nameimage = val.Nameimage
				objImage.Url = val.Url
				objImage.Filename = val.Filename
				objImage.Description = val.Description
				objImages = append(objImages,objImage)
			}
			obj.Image = objImages
			objs = append(objs,obj)
		}

		jsonResp,err := json.Marshal(objs)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`,err.Error())
			return
		}

		jsonResp = bytes.Replace(jsonResp, []byte("\\u003c"), []byte("<"), -1)
		jsonResp = bytes.Replace(jsonResp, []byte("\\u003e"), []byte(">"), -1)
		jsonResp = bytes.Replace(jsonResp, []byte("\\u0026"), []byte("&"), -1)

		code = http.StatusOK
		message = string(jsonResp)
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func findByUser(db *sql.DB,minioClient *minio.Client) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request) {
		code := http.StatusInternalServerError
		message := `{"message":"error get data"}`

		body,_ := ioutil.ReadAll(r.Body)
		idUser,_ := jsonparser.GetString(body,"publicKey")

		querySelect := fmt.Sprintf(`select ug.*,i.id as idImage,i.name,i.filename from tb_user_gifts ug LEFT JOIN tb_gifts g ON ug.id_gifts = g.id
LEFT JOIN tb_image i ON g.id_image = i.id where public_key = '%s';`,idUser)

		rows,err := db.Query(querySelect)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}
		defer rows.Close()

		var objs []UserGifts
		for rows.Next(){
			var obj UserGifts

			err := rows.Scan(&obj.Id,&obj.PublicKey,&obj.IdGifts,&obj.Total,&obj.Timestamp,&obj.IdImage,&obj.Name,&obj.Filename)
			if err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}

			url, err := minioClient.PresignedGetObject(BUCKET_NAME, obj.Filename, EXP_TIME, nil)
			if err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}

			obj.Url = url.String()

			objs = append(objs,obj)
		}

		jsonResp,err := json.Marshal(objs)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`,err.Error())
			return
		}
		code = http.StatusOK
		message = string(jsonResp)
		w.WriteHeader(code)
		w.Write([]byte(message))
	}
}

func findLastGiftByUser(db *sql.DB) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request) {
		code := http.StatusNotFound

		var id int64

		body,_ := ioutil.ReadAll(r.Body)
		idUser,_ := jsonparser.GetString(body,"publicKey")
		idGifts,_ := jsonparser.GetString(body,"idGifts")

		querySelect := fmt.Sprintf(`select IFNULL(id,0) as id from tb_user_gifts ug where ug.public_key =
'%s' and ug.id_gifts = %s ORDER BY timestamp DESC limit 1; `,idUser,idGifts)

		row := db.QueryRow(querySelect)

		err := row.Scan(&id)
		if err != nil {
			w.WriteHeader(code)
			return
		}

		if(id > 0){
			code = http.StatusOK
			w.WriteHeader(code)
		}else{
			w.WriteHeader(code)
		}
	}
}

func addImage(db *sql.DB,minioClient *minio.Client) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request) {
		code :=http.StatusInternalServerError
		message := `{"message":"Service Not Available"}`

		idLocation := r.FormValue("idLocation")
		name := r.FormValue("name")
		desc := r.FormValue("desc")
		file,header,_ := r.FormFile("image")

		if len(name) <= 0 || len(header.Filename) <= 0 || len(idLocation) <= 0{
			code = http.StatusBadRequest
			message = fmt.Sprintf(`{"message":"Error : %s"}`,"Payload is blank or not complete")
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}

		var url *url.URL

		contentType := header.Header.Get("Content-Type")
		filename := fmt.Sprintf("%d_%s", time.Now().Unix(), header.Filename)
		folder := "temp/"
		tmpPath := fmt.Sprintf("%s%s", folder, filename)
		f, err := os.Create(tmpPath)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}
		defer f.Close()
		io.Copy(f, file)

		_, err = minioClient.FPutObject(BUCKET_NAME, filename, tmpPath, minio.PutObjectOptions{ContentType: contentType})
		if err == nil {
			url, err = minioClient.PresignedGetObject(BUCKET_NAME, filename, EXP_TIME, nil)
			if err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}
			unixTime := time.Now().Unix()
			query := `INSERT INTO tb_image (url,name,filename,description,timestamps)
			VALUES ($1, $2, $3, $4, $5)`
			_,err = db.Exec(query,url.String(),name,filename,desc,unixTime)
			if err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}
			querySelect := `INSERT INTO tb_gifts (id_location,id_image) select $1,id from tb_image where name = $2 and timestamps=$3`
			_,err = db.Exec(querySelect,idLocation,name,unixTime)
			if err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}
		}else{
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}
		os.Remove(tmpPath)
		code = http.StatusCreated
		w.WriteHeader(code)
	}
}

func updateImage(db *sql.DB,minioClient *minio.Client) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request) {
		code :=http.StatusInternalServerError
		message := `{"message":"Service Not Available"}`

		id := r.FormValue("id")
		name := r.FormValue("name")
		desc := r.FormValue("desc")
		file,header,_ := r.FormFile("image")
		if len(name) <= 0 || len(header.Filename) <= 0 || len(id) <= 0{
			code = http.StatusBadRequest
			message = fmt.Sprintf(`{"message":"Error : %s"}`,"Payload is blank or not complete")
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}
		var url *url.URL


		contentType := header.Header.Get("Content-Type")
		filename := fmt.Sprintf("%d_%s", time.Now().Unix(), header.Filename)
		folder := "temp/"
		tmpPath := fmt.Sprintf("%s%s", folder, filename)
		f, err := os.Create(tmpPath)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}
		defer f.Close()
		io.Copy(f, file)

		_, err = minioClient.FPutObject(BUCKET_NAME, filename, tmpPath, minio.PutObjectOptions{ContentType: contentType})
		if err == nil {
			url, err = minioClient.PresignedGetObject(BUCKET_NAME, filename, EXP_TIME, nil)
			if err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}

			unixTime := time.Now().Unix()
			query := `UPDATE tb_image SET url = $1, name = $2,filename = $3,description = $4,timestamps = $5 WHERE id = $6)`
			_,err = db.Exec(query,url.String(),name,filename,desc,unixTime,id)
			if err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}
		}
		os.Remove(tmpPath)
		code = http.StatusOK
		w.WriteHeader(code)
	}
}

func removeImage(db *sql.DB,minioClient *minio.Client) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request) {
		code :=http.StatusInternalServerError
		message := `{"message":"Service Not Available"}`

		id := r.FormValue("id")
		if len(id) <= 0{
			code = http.StatusBadRequest
			message = fmt.Sprintf(`{"message":"Error : %s"}`,"Payload is blank or not complete")
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}

		var filename string
		query := `SELECT filename FROM tb_image WHERE id = ?`

		statement, err := db.Prepare(query)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`,err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}

		defer statement.Close()

		err = statement.QueryRow(id).Scan(&filename)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`,err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}

		err = minioClient.RemoveObject(BUCKET_NAME,filename)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`,err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}
		code = http.StatusOK
		w.WriteHeader(code)
	}
}

func setGiftsLocation(db *sql.DB,minioClient *minio.Client) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request) {
		code :=http.StatusInternalServerError
		message := `{"message":"Service Not Available"}`

		body,_ := ioutil.ReadAll(r.Body)
		obj := Gifts{}
		if err := json.Unmarshal(body,&obj);err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}

		queryDel := fmt.Sprintf(`DELETE FROM tb_gifts WHERE id_location = '%s' `,obj.IdLocation)
		if _,err := db.Exec(queryDel);err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}

		query := `INSERT INTO tb_gifts (id_location,id_image)
		VALUES ($1, $2)`
		for _,v := range obj.IdImage {
			if _,err := db.Exec(query,obj.IdLocation,v);err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}
		}

		code = http.StatusCreated
		w.WriteHeader(code)
	}
}

func setGiftUsers(db *sql.DB,minioClient *minio.Client) func(w http.ResponseWriter, r *http.Request){
	return func(w http.ResponseWriter, r *http.Request) {
		code :=http.StatusInternalServerError
		message := `{"message":"Service Not Available"}`

		body,_ := ioutil.ReadAll(r.Body)
		obj := UserGifts{}
		if err := json.Unmarshal(body,&obj);err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}

		querySelect := fmt.Sprintf(`select count(1) from tb_user_gifts where public_key = '%s' and id_gifts = '%s' ;`,obj.PublicKey,obj.IdGifts)

		row,err := db.Prepare(querySelect)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}
		defer row.Close()

		var count int
		err = row.QueryRow().Scan(&count)
		if err != nil {
			message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
			w.WriteHeader(code)
			w.Write([]byte(message))
			return
		}

		if count > 0 {
			query := `UPDATE tb_user_gifts set total = total+1 where public_key = $1 and id_gifts = $2`
			if _,err := db.Exec(query,obj.PublicKey,obj.IdGifts);err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}
			code = http.StatusOK
		}else{
			unixTime := time.Now().Unix()
			query := `INSERT INTO tb_user_gifts (public_key,id_gifts,total,timestamp)
		VALUES ($1, $2, 1, $3)`
			if _,err := db.Exec(query,obj.PublicKey,obj.IdGifts, unixTime);err != nil {
				message = fmt.Sprintf(`{"message":"Error : %s"}`, err.Error())
				w.WriteHeader(code)
				w.Write([]byte(message))
				return
			}
			code = http.StatusCreated
		}
		w.WriteHeader(code)
	}
}

//using encode decode base 64
func imageUploader()func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		body,_ := ioutil.ReadAll(r.Body)
		img,_:= jsonparser.GetString(body,"data")
		data, _ := base64.StdEncoding.DecodeString(img)
		f, err := os.Create("oke.jpg")
		if err != nil {
			panic(err)
		}
		defer f.Close()

		if _, err := f.Write(data); err != nil {
			panic(err)
		}
		if err := f.Sync(); err != nil {
			panic(err)
		}
		fmt.Println(data)
	}
}

func imageUploaderBinaryFile()func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		file,header,_ := r.FormFile("photo")
		tmpPath := fmt.Sprintf("temp/%s",header.Filename)
		f, err := os.Create(tmpPath)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		io.Copy(f, file)
	}
}